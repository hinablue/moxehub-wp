FROM ubuntu:bionic

ENV LANG=C.UTF-8

RUN set -x \
	&& apt-get update \
    && apt-get upgrade -y

RUN set -x \
	&& apt-get install --no-install-recommends --no-install-suggests -y \
    software-properties-common gnupg2 dirmngr curl git \
    language-pack-en-base openssl

RUN set -x \
    && apt-get install --no-install-recommends --no-install-suggests -y nginx-full mysql-client imagemagick

RUN echo "America/Los_Angeles" > /etc/localtime

RUN set -x \
    && apt-get install --no-install-recommends --no-install-suggests -y \
    php7.2-common php7.2-fpm php7.2-cli php7.2-dev php7.2-mbstring php7.2-curl \
    php7.2-intl php7.2-mysql php7.2-opcache php7.2-soap php7.2-readline php7.2-xml php7.2-zip \
    php-imagick php-memcached php-redis

RUN set -x \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/sbin \
    && ln -svf /usr/local/sbin/composer.phar /usr/local/sbin/composer

RUN echo "localhost localhost.localdomain" >> /etc/hosts

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/moxehub.com.conf /etc/nginx/sites-enabled/moxehub.com.conf
COPY config/www.conf /etc/php/7.2/fpm/pool.d/www.conf
COPY config/php.ini /etc/php/7.2/fpm/php.ini
COPY config/start.sh start.sh

CMD ["bash", "start.sh"]
