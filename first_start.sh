#!/bin/bash

if [ ! -d "log/nginx" ]; then
  mkdir -p -m 0755 log/nginx
fi;

docker-compose up -d --force-recreate --build
