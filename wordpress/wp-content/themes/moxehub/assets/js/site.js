(function($, window, document, undefined) {
  /**
   * Homepage slider action.
   */
  var slider = $('#first-section-slider');
  var activedSlide = 0;
  if (slider.length > 0) {
    var slides = [];
    $('.slide', slider).each(function(key, item) {
      slides.push({
        actived: !!$(item).hasClass('actived'),
        background: $(item).data('background'),
        percentage: Math.round($(item).data('percentage')),
        belongTo: $(item).data('belong-to')
      });
      $(item).css({
        backgroundImage: 'url(' + $(item).data('background') + ')',
        transition: slider.data('transition')
      });
      if (!!$(item).hasClass('actived')) {
        activedSlide = key;
      }
    });
    $($('.slide', slider)[activedSlide]).addClass('actived');
    $('#' + slides[activedSlide].belongTo + '-progress-line')
      .removeClass('tc-dark-yellow');
    $('.progress-line', '#' + slides[activedSlide].belongTo + '-progress-line')
      .attr('data-percentage', slides[activedSlide].percentage);

    setInterval(function() {
      $($('.slide', slider)[activedSlide]).removeClass('actived');
      $('#' + slides[activedSlide].belongTo + '-progress-line')
        .addClass('tc-dark-yellow');
      $('.progress-line', '#' + slides[activedSlide].belongTo + '-progress-line')
        .attr('data-percentage', 0);

      activedSlide++;
      if (activedSlide > (slides.length - 1)) {
        activedSlide = 0;
      }

      $($('.slide', slider)[activedSlide]).addClass('actived');
      $('#' + slides[activedSlide].belongTo + '-progress-line')
        .removeClass('tc-dark-yellow');
      $('.progress-line', '#' + slides[activedSlide].belongTo + '-progress-line')
        .attr('data-percentage', slides[activedSlide].percentage);

    }, (Math.round((slider.data('during') || 3) * 1000)));
  }
  /**
   * Homepage services parallax scroll for title and items.
   */
  var scrollSpyOnHomepageServicesSection = function() {
    if ($('#services-section').length > 0) {
      var servicesSection = $('#services-section');
      if ($(this).scrollTop() > Math.round(servicesSection.offset().top)) {
        var offset = $(this).scrollTop() - Math.round(servicesSection.offset().top - $('#services-title').height());
        if (offset < 190 && $(window).width() >= 768) {
          $('#services-title').css('transform', 'translateY(' + offset + 'px)');
        }
      } else {
        $('#services-title').css('transform', 'translateY(0px)');
      }
    }
  };
  /**
   * Works form.
   */
  if ($('#works-form').length > 0) {
    var errorMessageTimer;
    $('#works-form').ajaxForm({
      beforeSubmit: function (data, form, optiosn) {
        $('#submit-button')
          .addClass('form-sending')
          .attr('disabled', true);
        return true;
      },
      error: function (xhr, status, errorText, form) {
        var errorMessage = 'Sorry, we cannot process your submit, please try again later.';
        if (typeof xhr.responseJSON.message !== 'undefined') {
          errorMessage = xhr.responseJSON.message;
        }
        $('.error-message', '#error-message').text(errorMessage);
        $('#error-message').show();
        $('#submit-button')
          .removeClass('form-sending')
          .attr('disabled', false);
        if (typeof errorMessageTimer !== 'undefined') {
          clearTimeout(errorMessageTimer);
        }
        errorMessageTimer = setTimeout(function() {
          $('#error-message').hide();
        }, 3000);
      },
      success: function (res, status, xhr, form) {
        $('#submit-button')
          .removeClass('form-sending')
          .addClass('form-sending-succeeded')
          .attr('disabled', false);
        if (typeof errorMessageTimer !== 'undefined') {
          clearTimeout(errorMessageTimer);
        }
        var errorMessage = 'Sorry, we cannot process your submit, please try again later.';
        $('.error-message', '#error-message').text(errorMessage);
        $('#error-message').show();
        errorMessageTimer = setTimeout(function() {
          $('#submit-button').removeClass('form-sending-succeeded');
        }, 1500);
        return true;
      }
    });
  }
  /**
   * Contact us form.
   */
  if ($('#contact-us-form').length > 0) {
    var errorMessageTimer;
    $('#contact-us-form').ajaxForm({
      beforeSubmit: function (data, form, optiosn) {
        $('#submit-button')
          .addClass('form-sending')
          .attr('disabled', true);
        return true;
      },
      error: function (xhr, status, errorText, form) {
        var errorMessage = 'Sorry, we cannot process your submit, please try again later.';
        if (typeof xhr.responseJSON.message !== 'undefined') {
          errorMessage = xhr.responseJSON.message;
        }
        $('.error-message', '#error-message').text(errorMessage);
        $('#error-message').show();
        $('#submit-button')
          .removeClass('form-sending')
          .attr('disabled', false);
        if (typeof errorMessageTimer !== 'undefined') {
          clearTimeout(errorMessageTimer);
        }
        errorMessageTimer = setTimeout(function() {
          $('#error-message').hide();
        }, 3000);
      },
      success: function (res, status, xhr, form) {
        $('#submit-button')
          .removeClass('form-sending')
          .addClass('form-sending-succeeded')
          .attr('disabled', false);
        if (typeof errorMessageTimer !== 'undefined') {
          clearTimeout(errorMessageTimer);
        }
        errorMessageTimer = setTimeout(function() {
          $('#submit-button').removeClass('form-sending-succeeded');
        }, 1500);
        // google landing page Conversion Page
        /* <![CDATA[ */
          goog_snippet_vars = function() {
            var w = window;
            w.google_conversion_id = 801248031;
            w.google_conversion_label = "UwpNCJbyh4YBEJ-miP4C";
            w.google_remarketing_only = false;
          }
          // DO NOT CHANGE THE CODE BELOW.
          goog_report_conversion = function(url) {
            goog_snippet_vars();
            window.google_conversion_format = "3";
            var opt = new Object();
            opt.onload_callback = function() {
            if (typeof(url) != 'undefined') {
              window.location = url;
            }
          }
          var conv_handler = window['google_trackConversion'];
          if (typeof(conv_handler) == 'function') {
            conv_handler(opt);
          }
          /* ]]> */
        }
        goog_report_conversion();
        return true;
      }
    });
    /**
     * Progress animation.
     */
    setTimeout(function() {
      $('.progress-circle .progress-value').each(function() {
        var self = $(this);
        self.css('stroke-dashoffset', self.data('stroke-dashoffset'));
      });
    }, 1500);
  }
  /**
   * Menu Button
   */
  $('#menu-button').on('click', function() {
    var menu = $('#menu-full-screen');
    menu.addClass('menu-show');
    setTimeout(function() {
      menu.addClass('menu-show-in');
      $(window).scrollTop(0);
    }, 30);
  });
  $('#close-menu-button').on('click', function() {
    var menu = $('#menu-full-screen');
    menu.removeClass('menu-show-in');
    setTimeout(function() {
      menu.removeClass('menu-show');
      $(window).scrollTop(0);
    }, 100);
  });
  /**
   * Google Map
   */
  window.initGoogleMap = function () {
    var mapContainer = document.getElementById('google-map');
    if (mapContainer) {
      var map = new google.maps.Map(mapContainer, {
        center: new google.maps.LatLng(42.3499949,-71.0788503),
        zoom: 17,
        styles: [
          {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
          {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
          {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
          {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#263c3f'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#6b9a76'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#38414e'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{color: '#212a37'}]
          },
          {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9ca5b3'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#746855'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#1f2835'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#f3d19c'}]
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{color: '#2f3948'}]
          },
          {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#17263c'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#515c6d'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#17263c'}]
          }
        ]
      });
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(42.3499949,-71.0788503),
        icon: window.GOOGLE_MAP_MARKER,
        map: map
      });
    }
  };
  /**
   * Reset Google Map Size.
   */
  var resetGoogleMapSize = function() {
    var googleMap = $('#google-map');
    if (googleMap.length > 0) {
      var windowWidth = $(this).width();
      googleMap.height(Math.round(472 / 1442 * windowWidth));
    }
  };
  /**
   * Services page animation.
   */
  var serviceItemTarget = $('main.services .services-item');
  var servicesItemBackgroundImage = {
    'the-niche': '',
    'software-development': '',
    'design-consulting': '',
    'program-management-augmentation': ''
  };
  if (serviceItemTarget.length > 0) {
    var servicesItemTransition;
    $.each(Object.keys(servicesItemBackgroundImage), function(i, key) {
      var target = $('main.services .services-item.' + key);
      if (target) {
        servicesItemBackgroundImage[key] = target.css('backgroundImage');
        target.on('mouseenter', function() {
          if ($(window).width() >= 992) {
            var self = this;
            servicesItemTransition = setTimeout(function() {
              serviceItemTarget.addClass('hover-extra');
              $(self).addClass('hover-extra-self');
              serviceItemTarget.css('backgroundImage', $(self).css('backgroundImage'));
            }, 310);
          }
        }).on('mouseleave', function() {
          if ($(window).width() >= 992) {
            clearTimeout(servicesItemTransition);
            $.each(Object.keys(servicesItemBackgroundImage), function(i, k) {
              var target = $('main.services .services-item.' + k);
              if (target) {
                target.css('backgroundImage', servicesItemBackgroundImage[k]);
              }
            });
            serviceItemTarget.removeClass('hover-extra').removeClass('hover-extra-self');
          }
        });
      }
    });
    /**
     * Service page detail button action.
     */
    var servicesModalTransition;
    $('main.services .services-item .detail-button').on('click', function() {
      clearTimeout(servicesModalTransition);
      var target = $(this).data('target');
      $('main.services .services-item').toggleClass('modal-open');
      $('main.services .services-modal').removeClass('modal-open');
      $('main.services .services-modal.' + target).toggleClass('modal-open');
      servicesModalTransition = setTimeout(function() {
        $('main.services .services-modal.' + target).toggleClass('modal-in');
        var firstRow = $('main.services > .row');
        var header = $('header nav.navbar');
        $(window).scrollTop(firstRow.offset().top + firstRow.height() - header.height() - 24);
        setTimeout(function() {
          scrollSpyOnHeader();
        }, 100);
      }, 100);
    });
    $('main.services .services-modal .close-modal-button').on('click', function() {
      clearTimeout(servicesModalTransition);
      $('main.services .services-modal').removeClass('modal-in');
      servicesModalTransition = setTimeout(function() {
        $('main.services .services-modal').removeClass('modal-open');
        $('main.services .services-item').removeClass('modal-open');
      }, 100);
    });
  }

  /**
   * Scroll spy on header menu.
   */
  var scrollSpyOnHeader = function () {
    var scrollTop = $(this).scrollTop();
    var header = $('header nav.navbar');
    var scrollTrigger = header.data('on-scroll') || 75;

    if (scrollTop >= scrollTrigger) {
      if (header.hasClass('navbar-transparent')) {
        header.removeClass('navbar-transparent');
      }
    } else {
      if (!header.hasClass('navbar-transparent')) {
        header.addClass('navbar-transparent');
      }
    }
  };

  /**
   * Dynamic reset second section height.
   */
  var setSecondSectionHeight = function () {
    var secondSection = $('#second-section');
    if (secondSection.length > 0) {
      var windowWidth = $(this).width();
      secondSection.height(Math.round(453 / 603 * windowWidth - windowWidth * 0.14));
    }
  };
  var scrollSpyOnHomepageSecondSection = function() {
    var stock = $('#second-section > .shutter_stock');
    if (stock.length > 0) {
      var scrollTop = $(this).scrollTop();
      var scrollTrigger = Math.round($(this).outerHeight() / 3);
      if (scrollTop >= scrollTrigger) {
        if (!stock.hasClass('stock-in')) {
          stock.addClass('stock-in');
        }
      }
    }
  };

  /**
   * Dynamic calculate services item height.
   */
  var setServicesItemHeight = function () {
    var item = $('main.services .services-item');
    if (item.length > 0) {
      var maxDataHeight = 0;
      var servicesItemWidth = item.width();
      var __height = Math.round(servicesItemWidth * 368 / 553);
      item.each(function() {
        maxDataHeight = Math.max(
          $('.services-item-data', item).height() - 20
          , maxDataHeight
        );
        $(this).height(__height <= 350 ? 'auto' : __height);
        if (__height <= 350) {
          $('.services-item-data').css('transform', 'translateY(0rem)');
        }
      });
      if (__height > 350) {
        item.each(function() {
          $('.services-item-data').css(
            'transform',
            'translateY(' + Math.round((__height - $('.align-self-end', item).height() - maxDataHeight)) + 'px)'
          );
        });
      }
    }
  };

  window.addEventListener('resize', function() {
    setSecondSectionHeight.call(this);
    setServicesItemHeight.call(this);
    resetGoogleMapSize.call(this);
  }, false);

  window.addEventListener('scroll', function() {
    scrollSpyOnHeader.call(this);
    scrollSpyOnHomepageSecondSection.call(this);
    scrollSpyOnHomepageServicesSection.call(this);
  }, false);

  setSecondSectionHeight();
  setServicesItemHeight();
  resetGoogleMapSize();

  stickyFooter.init();
})(jQuery, window, document);
