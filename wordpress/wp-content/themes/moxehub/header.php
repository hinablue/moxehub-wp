<?php
/**
 * The header template file
 *
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <meta itemprop="name" content="MoxeHub">
    <meta itemprop="image" content="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/fb_og.png">
    <meta itemprop="description" content="MoxeHub provides digital solutions for App & Web development">

    <meta property="og:image" content="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/fb_og.png">
    <meta property="og:title" content="Welcome to MoxeHub. The home of Disruptive Innovation"/>
    <meta property="og:description" content="MoxeHub provides digital solutions for App & Web development">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://moxehub.com/"/>
    <meta property="og:site_name" content="MoxeHub"/>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Welcome to MoxeHub.">
    <meta name="twitter:description" content="MoxeHub provides digital solutions for App & Web development">
    <meta name="twitter:image:src" content="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/fb_og.png">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120058618-1"></script>
    <script>
      window.GOOGLE_MAP_MARKER = '<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/google_map_marker.png';

      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120058618-1');
    </script>
    <?php wp_head(); ?>
</head>

<body <?php
if (!is_home() && is_front_page()) {
    body_class(['moxehub-homepage', 'moxehub-layout-transparent']);
} else {
    $slug = get_post_field('post_name', get_post());
    switch ($slug) {
    case 'contact':
        body_class(['moxehub-contact-us']);
        break;
    case 'services':
        body_class(['moxehub-services']);
        break;
    case 'works':
        body_class(['moxehub-works']);
        break;
    }
} ?>>
<header class="container-fluid">
  <nav class="navbar navbar-color-on-scroll navbar-transparent navbar-expand-lg fixed-top" data-on-scroll="100">
    <div class="container">
      <div class="row">
        <div class="navbar-translate">
          <button type="button" class="btn btn-link menu-button" id="menu-button">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/menu_bar.svg" title="Menu" alt="Menu" />
            <span>Menu</span>
          </button>
          <a class="btn btn-link header-logo-button" href="/">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/header_logo.svg" title="MoxeHub" alt="MoxeHub" />
          </a>
        </div>
      </div>
    </div>
  </nav>
</header>
