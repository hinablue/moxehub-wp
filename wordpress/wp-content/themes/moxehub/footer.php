<?php
/**
 * The footer template file
 *
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
?>

<footer class="footer container-fluid" data-sticky-footer>
  <section class="row no-gutters">
    <div class="col-sm-6 align-self-start logo-and-navigation">
<a href="/" class="btn btn-fab btn-round footer-logo">        <img class="footer-logo__icon" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/footer_logo.svg" />
</a>      <nav class="navigation">
  <a href="/" class="btn btn-link">Home</a>
  <a href="/services/" class="btn btn-link">Services</a>
  <a href="/works/" class="btn btn-link">Projects</a>
  <a href="/contact/" class="btn btn-link">Contact</a>
</nav>

    </div>
    <div class="col-sm-6 align-self-end address-and-copyright">
      <div class="">
        <ul class="sso-links">
  <li>
<a href="https://www.facebook.com/MoxeHub/" class="btn btn-link" target="_blank">      <i class="fab fa-facebook-f"></i>
</a>  </li>
  <li>
<a href="https://twitter.com/moxehub" class="btn btn-link" target="_blank">      <i class="fab fa-twitter"></i>
</a>  </li>
  <li>
<a href="https://www.instagram.com/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-instagram"></i>
</a>  </li>
  <li>
<a href="https://www.linkedin.com/company/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-linkedin"></i>
</a>  </li>
  <li>
<a href="https://www.pinterest.com/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-pinterest"></i>
</a>  </li>
</ul>
      </div>
      <p class="copyright">
        <span class="address">
          Back Bay, Boston<br>
          <a href="mailto:ivan.reilly@moxehub.com">ivan.reilly@moxehub.com</a>
        </span>
        <br>
        <span>
          &copy;
          <script>document.write(new Date().getFullYear());</script>,
          Moxehub. All rights reserved.
        </span>
      </p>
    </div>
  </section>
</footer>
<div class="menu-full-screen" id="menu-full-screen">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <a class="btn btn-link header-logo-button" href="/">
          <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/header_logo.svg" title="MoxeHub" alt="MoxeHub" />
        </a>
      </div>
      <div class="col">
        <nav class="navigation">
  <a href="/" class="btn btn-link">Home</a>
  <a href="/services/" class="btn btn-link">Services</a>
  <a href="/works/" class="btn btn-link">Projects</a>
  <a href="/contact/" class="btn btn-link">Contact</a>
</nav>

      </div>
      <div class="col follow-us">
        <div class="">
          <h2>Follow Us</h2>
          <ul class="sso-links">
  <li>
<a href="https://www.facebook.com/MoxeHub/" class="btn btn-link" target="_blank">      <i class="fab fa-facebook-f"></i>
</a>  </li>
  <li>
<a href="https://twitter.com/moxehub" class="btn btn-link" target="_blank">      <i class="fab fa-twitter"></i>
</a>  </li>
  <li>
<a href="https://www.instagram.com/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-instagram"></i>
</a>  </li>
  <li>
<a href="https://www.linkedin.com/company/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-linkedin"></i>
</a>  </li>
  <li>
<a href="https://www.pinterest.com/moxehub/" class="btn btn-link" target="_blank">      <i class="fab fa-pinterest"></i>
</a>  </li>
</ul>
        </div>
      </div>
    </div>
  </div>
  <button class="btn btn-link close-menu-button" id="close-menu-button">
    <i class="material-icons">close</i>
  </button>
</div>
<?php wp_footer(); ?>
<script type="text/javascript"
    src="//www.googleadservices.com/pagead/conversion_async.js">
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7BUgQ3xGy14IdachgnlC7M3vJwSRW3p0&callback=initGoogleMap"
    async defer></script>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 801248031;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/801248031/?guid=ON&amp;script=0"/>
    </div>
</noscript>
</body>
</html>
