<?php
/**
 * The functions file
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */

/**
 * Theme steup
 *
 * @return null
 */
function moxehubSetup()
{
    load_theme_textdomain('moxehub');
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'moxehubSetup');

/**
 * Set front-page function
 *
 * @param string $template Template content
 *
 * @return string
 */
function moxehubFrontPageTemplate($template)
{
    return is_home() ? '' : $template;
}

add_filter('frontpage_template',  'moxehubFrontPageTemplate');

/**
 * Email function
 *
 * @return null
 */
function sendContactEmail()
{
    $smtp_server = 'smtp.gmail.com';
    $smtp_port = '465';
    $smtp_security = 'ssl';
    $smtp_username = 'moxehub.system';
    $smtp_password = 'moxehub0425';

    if (!isset($_POST['name'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Name is required'
            ]
        );
        wp_die();
    }
    if (!isset($_POST['email'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'E-mail is required'
            ]
        );
        wp_die();
    }
    if (!isset($_POST['phone'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Phone is required'
            ]
        );
        wp_die();
    }
    if (!isset($_POST['messages'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Please leave your message'
            ]
        );
        wp_die();
    }
    if (empty($_POST['name'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Name is required'
            ]
        );
        wp_die();
    }
    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'E-mail is invalid, please check your E-mail address'
            ]
        );
        wp_die();
    }
    if (empty($_POST['phone'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Phone is required'
            ]
        );
        wp_die();
    }
    if (empty($_POST['messages'])) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Message is required'
            ]
        );
        wp_die();
    }

    $ret = [];
    $ret['name'] = trim($_POST['name']);
    $ret['phone'] = trim($_POST['phone']);
    $ret['email'] = trim($_POST['email']);
    $ret['messages'] = nl2br(trim($_POST['messages']));

    $curr_time = new DateTime('now');
    $subject = '[moxehub] web site messages ' . $curr_time->format('Y-m-d H:i:s');
    $to = ['moxehub@gmail.com'];
    $template = <<<HTML
Name: {$ret['name']} <br><br>
Phone: {$ret['phone']} <br><br>
Email: {$ret['email']} <br><br>
Message:<br>
{$ret['messages']}
HTML;

    $from = $_POST['email'];
    try {
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom($from);

        $message->setBody($template, 'text/html');

        $transport = Swift_SmtpTransport::newInstance(
            $smtp_server,
            $smtp_port,
            $smtp_security
        )->setUsername($smtp_username)->setPassword($smtp_password);

        $mailer = Swift_Mailer::newInstance($transport);
        if ($mailer->send($message)) {
            echo json_encode(
                [
                    'status' => 'ok',
                    'messages' => 'Succeeded'
                ]
            );
        } else {
            echo json_encode(
                [
                    'status' => 'error',
                    'messages' => 'Sorry, we cannot process your submit, please try again later.'
                ]
            );
        }
    } catch (\Exception $e) {
        echo json_encode(
            [
                'status' => 'error',
                'messages' => 'Sorry, we cannot process your submit, please try again later.'
            ]
        );
    }
    wp_die();
}

add_action('wp_ajax_contact_sned_email', 'sendContactEmail');
add_action('wp_ajax_nopriv_contact_sned_email', 'sendContactEmail');

/**
 * Enqueue scripts and styles.
 *
 * @return null
 */
function moxehubScripts()
{
    wp_enqueue_style('es5-shim', get_theme_file_uri('/assets/js/es5-shim.js'), array('moxehub-style'));
    wp_enqueue_style('es5-sham', get_theme_file_uri('/assets/js/es5-sham.js'), array('moxehub-style'));
    wp_enqueue_style('es6-shim', get_theme_file_uri('/assets/js/es6-shim.js'), array('moxehub-style'));
    wp_enqueue_style('es6-sham', get_theme_file_uri('/assets/js/es6-sham.js'), array('moxehub-style'));
    wp_enqueue_style('es7-shim', get_theme_file_uri('/assets/js/es7-shim.js'), array('moxehub-style'));
    wp_style_add_data('es5-shim', 'conditional', 'lte IE 10');
    wp_style_add_data('es5-sham', 'conditional', 'lte IE 10');
    wp_style_add_data('es6-shim', 'conditional', 'lte IE 10');
    wp_style_add_data('es6-sham', 'conditional', 'lte IE 10');
    wp_style_add_data('es7-shim', 'conditional', 'lte IE 10');

    wp_enqueue_style('fontawesome', get_theme_file_uri('/assets/css/fontawesome.css'), array(), '5.1.0');
    wp_enqueue_style('material-kit', get_theme_file_uri('/assets/css/material-kit.css'), array(), '2.0.2');
    wp_enqueue_style('moxehub-style', get_stylesheet_uri());

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_theme_file_uri('/assets/js/core/jquery.min.js'), array(), '3.2.1', true);
    wp_enqueue_script('popper', get_theme_file_uri('/assets/js/core/popper.min.js'), array(), false, true);
    wp_enqueue_script('bootstrap-material-design', get_theme_file_uri('/assets/js/bootstrap-material-design.min.js'), array(), false, true);
    wp_enqueue_script('moment', get_theme_file_uri('/assets/js/plugins/moment.min.js'), array(), false, true);
    wp_enqueue_script('bootstrap-datetimepicker', get_theme_file_uri('/assets/js/plugins/bootstrap-datetimepicker.min.js'), array(), false, true);
    wp_enqueue_script('nouislider', get_theme_file_uri('/assets/js/plugins/nouislider.min.js'), array(), false, true);
    wp_enqueue_script('jquery.form', get_theme_file_uri('/assets/js/plugins/jquery.form.min.js'), array(), '4.2.2', true);
    wp_enqueue_script('sticky-footer', get_theme_file_uri('/assets/js/plugins/sticky-footer.min.js'), array(), '4.2.0', true);
    wp_enqueue_script('material-kit.js', get_theme_file_uri('/assets/js/material-kit.js'), array(), '2.0.2', true);
    wp_enqueue_script('moxehub-site', get_theme_file_uri('/assets/js/site.js'), array(), false, true);
}

add_action('wp_enqueue_scripts', 'moxehubScripts');

?>