<?php
/**
 * The contact
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */

$content = nl2br(get_the_content());
?>

<main class="container-fluid contact-us" data-sticky-wrap>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10">
        <h1><?php echo get_the_title(); ?></h1>
        <h2>Meet our Founder & CEO</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-10">
        <div class="card ceo-card">
          <div class="row">
            <div class="col-12 col-sm-5 ceo-card-image" style="background-image:url(<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ceo_image.png)"></div>
            <div class="col-12 col-sm-7 ceo-card-information">
              <div class="row justify-content-center row__max-height">
                <div class="col-12 col-md-10 ">
                  <div class="row row__max-height">
                    <div class="col-12 card-information">
                      <div class="row justify-content-start">
                        <div class="col-4 col-md-4 col-lg-3">Web</div>
                        <div class="col-8 col-md-7 col-lg-8"><a href="https://www.moxehub.com"> www.moxehub.com</a></div>
                      </div>

                      <div class="row justify-content-start">
                        <div class="col-4 col-md-4 col-lg-3">Mail</div>
                        <div class="col-8 col-md-7 col-lg-8"><a href="mailto:ivan.reilly@moxehub.com"> ivan.reilly@moxehub.com</a></div>
                      </div>

                      <div class="row justify-content-start">
                        <div class="col-4 col-md-4 col-lg-3">linkedin</div>
                        <div class="col-8 col-md-7 col-lg-8"><a href="https://www.linkedin.com/in/ivanreilly" target="_blank"> www.linkedin.com/in/ivanreilly</a></div>
                      </div>

                    </div>
                    <div class="col-12 align-self-start ceo-card-sso ">
                      <ul class="sso-links offset-sm-4 offset-lg-3">
                        <li>
<a href="https://www.facebook.com/MoxeHub/" class="btn btn-link" target="_blank">                            <i class="fab fa-facebook-f"></i>
</a>                        </li>
                        <li>
<a href="https://twitter.com/moxehub" class="btn btn-link" target="_blank">                            <i class="fab fa-twitter"></i>
</a>                        </li>
                        <li>
<a href="https://www.instagram.com/moxehub/" class="btn btn-link" target="_blank">                            <i class="fab fa-instagram"></i>
</a>                        </li>
                        <li>
<a href="https://www.linkedin.com/company/moxehub/" class="btn btn-link" target="_blank">                            <i class="fab fa-linkedin"></i>
</a>                        </li>
                        <li>
<a href="https://www.pinterest.com/moxehub/" class="btn btn-link" target="_blank">                            <i class="fab fa-pinterest"></i>
</a>                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-10">
        <div class="row">
          <div class="col-12 ceo-description">
            <h4>Ivan Reilly</h4>
            <section><?php echo $content; ?></section>
            <div class="row">
              <blockquote class="col-12 col-lg-10 offset-lg-1">
                <p class="font-italic font-weight-bold">“We want to work with clients on projects that truly impact people’s lives.<br>That’s the WHY of MoxēHub</h6>”<p>- Ivan Reilly</p>
              </blockquote>
            </div>
          </div>
          <div class="col-12">
            <div class="row">
              <div class="col-12 col-sm-6 col-lg-3 progress-circle-container">
                <svg class="progress-circle software-app-development" width="160" height="160" viewBox="0 0 160 160">
                    <circle cx="80" cy="80" r="73" fill="none" stroke="#F4F4F4" stroke-width="14" />
                    <circle cx="80" cy="80" r="73" fill="none" class="progress-value" stroke="#96822F" stroke-width="14" data-stroke-dashoffset="45.8672" />
                </svg>
                <div class="row justify-content-center align-items-center progress-circle-name middle-progress">
                  <div class="col percentage">90%</div>
                  <div class="col name">Application Development</div>
                </div>
                <p class="progress-circle-name bottom-name">
                  <span>Application Development</span>
                </p>
              </div>
              <div class="col-12 col-sm-6 col-lg-3 progress-circle-container">
                <svg class="progress-circle healthcare-medical-devices" width="160" height="160" viewBox="0 0 160 160">
                    <circle cx="80" cy="80" r="73" fill="none" stroke="#F4F4F4" stroke-width="14" />
                    <circle cx="80" cy="80" r="73" fill="none" class="progress-value" stroke="#96822F" stroke-width="14" data-stroke-dashoffset="183.4688" />
                </svg>
                <div class="row justify-content-center align-items-center progress-circle-name middle-progress">
                  <div class="col percentage">60%</div>
                  <div class="col name">Healthcare / Medical Devices</div>
                </div>
                <p class="progress-circle-name bottom-name">
                  <span>Healthcare / Medical Devices</span>
                </p>
              </div>
              <div class="col-12 col-sm-6 col-lg-3 progress-circle-container">
                <svg class="progress-circle program-management" width="160" height="160" viewBox="0 0 160 160">
                    <circle cx="80" cy="80" r="73" fill="none" stroke="#F4F4F4" stroke-width="14" />
                    <circle cx="80" cy="80" r="73" fill="none" class="progress-value" stroke="#96822F" stroke-width="14" data-stroke-dashoffset="91.7344" />
                </svg>
                <div class="row justify-content-center align-items-center progress-circle-name middle-progress">
                  <div class="col percentage">80%</div>
                  <div class="col name">Program Management</div>
                </div>
                <p class="progress-circle-name bottom-name">
                  <span>Program Management</span>
                </p>
              </div>
              <div class="col-12 col-sm-6 col-lg-3 progress-circle-container">
                <svg class="progress-circle operational-excellence" width="160" height="160" viewBox="0 0 160 160">
                    <circle cx="80" cy="80" r="73" fill="none" stroke="#F4F4F4" stroke-width="14" />
                    <circle cx="80" cy="80" r="73" fill="none" class="progress-value" stroke="#96822F" stroke-width="14" data-stroke-dashoffset="45.8672" />
                </svg>
                <div class="row justify-content-center align-items-center progress-circle-name middle-progress">
                  <div class="col percentage">90%</div>
                  <div class="col name">Operational Excellence</div>
                </div>
                <p class="progress-circle-name bottom-name">
                  <span>Operational Excellence</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="google-map" id="google-map"></div>
  <div class="lets-code">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-10">
          <p>Let's Code !</p>
        </div>
      </div>
    </div>
  </div>
  <form class="contact-us-form" id="contact-us-form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST">
    <input type="hidden" name="action" value="contact_sned_email">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-10">
          <div class="row">
            <div class="col-12">
              <h3>Contact Us :</h3>
            </div>
            <div class="col-12 col-sm-6">
              <p>Address  Back Bay, Boston</p>
            </div>
            <div class="col-12 col-sm-6">
            </div>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                  <label for="user-name" class="bmd-label-floating">Name</label>
                  <input type="text" class="form-control" id="user-name" name="name" required>
              </div>
            </div>
            <div class="col-12 col-sm-6">
              <div class="form-group">
                  <label for="user-phone" class="bmd-label-floating">Phone</label>
                  <input type="text" class="form-control" id="user-phone" name="phone" required>
                  <span class="bmd-help">We'll never share your phone with anyone else.</span>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                  <label for="user-email" class="bmd-label-floating">E-mail</label>
                  <input type="email" class="form-control" id="user-email" name="email" required>
                  <span class="bmd-help">We'll never share your email with anyone else.</span>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                <label for="user-messages" class="bmd-label-floating">Messages</label>
                <textarea id="user-messages" class="form-control" name="messages" required></textarea>
              </div>
            </div>
            <div class="col-12 submit-button-container">
              <div class="alert alert-danger" id="error-message">
                <div class="container">
                  <div class="row">
                    <div class="col-1">
                      <div class="alert-icon">
                          <i class="material-icons">error_outline</i>
                      </div>
                    </div>
                    <div class="col-10">
                      <b>Error Alert:</b>
                      <span class="error-message"></span>
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-link submit-button" id="submit-button">
                <span class="send-text">Send</span>
                <div class="loading-icon">
                  <svg class="spinner" width="174px" height="174px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle class="path" fill="transparent" stroke-width="2" cx="33" cy="33" r="30" stroke="url(#gradient)"/>
                      <linearGradient id="gradient">
                        <stop offset="50%" stop-color="#96822F" stop-opacity="1"/>
                        <stop offset="65%" stop-color="#96822F" stop-opacity=".5"/>
                        <stop offset="100%" stop-color="#96822F" stop-opacity="0"/>
                      </linearGradient>
                    </circle>
                  </svg>
                </div>
                <div class="succeeded-icon"></div>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</main>