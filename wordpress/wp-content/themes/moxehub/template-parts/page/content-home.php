<?php
/**
 * The homepage
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
?>

<main class="container-fluid homepage" data-sticky-wrap>
  <div class="row first-section" id="first-section">
    <div class="first-section-slider" id="first-section-slider" data-during="4" data-transition="all 3s">
  <div class="slide" data-background="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/background.png" data-belong-to="left" data-percentage="100"></div>
  <div class="slide" data-background="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/background_4.png" data-belong-to="right" data-percentage="100"></div>
</div>
    <div class="col-12">
      <h1 class="homepage-logo">MoxeHub</h1>
      <h5 class="sub-title">A force of character, determination with gumption</h5>
    </div>
    <div class="col-12">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-10">
          <div class="row justify-content-center">
            <div class="col-8 footer__feature-is-now tc-dark-yellow" id="left-progress-line">
              <div class="progress-line" data-percentage="0"></div>
              <p>Disruptive Innovation Awaits</p>
            </div>
            <!-- <div class="col-6 footer__website tc-dark-yellow" id="right-progress-line">
              <div class="progress-line" data-percentage="0"></div>
              <p>website</p>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-sm-center align-items-center second-section" id="second-section">
    <div class="shutter_stock"></div>
    <div class="col-sm-8 col-md-6 col-lg-5 description">MoxēHub is a Boston based team of Application coding “Jedi”. We support our clients’ needs to build a better future for their business. We are dedicated to designing implementable software development strategies while pushing the limits of convention.</div>
  </div>

  <div class="row justify-content-center services-section" id="services-section">
    <div class="col-12 services-header">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-10">
          <h3 id="services-title">Services</h3>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-10 align-self-center">
      <div class="row no-gutters align-items-end service-items" id="service-items">
          <div class="col-12 col-sm-6 col-md-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/application_development.svg" class="item-icon application-development" alt="Application Development" />
            <p>Application Development</p>
          </div>
          <div class="col-12 col-sm-6 col-md-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/web_cloud_development.svg" class="item-icon web-cloud-development" alt="Web / Cloud Development" />
            <p>Web / Cloud Development</p>
          </div>
          <div class="col-12 col-sm-6 col-md-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/design_consulting.svg" class="item-icon design-consulting" alt="Design Consulting" />
            <p>Design Consulting</p>
          </div>
          <div class="col-12 col-sm-6 col-md-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/program_management.svg" class="item-icon program-management" alt="Program Management" />
            <p>Program Management</p>
          </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-center customer-segments-section">
    <div class="col-12 customer-segments-header">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-10">
          <h3>Customer Segments</h3>
        </div>
      </div>
    </div>
    <div class="col-12 align-self-center">
      <div class="row no-gutters align-items-start customer-segments-items">
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/healthcare_iot.png" class="item-image healthcare-iot" alt="Healthcare IoT" />
            <p>Healthcare IoT</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/education.png" class="item-image education" alt="Education" />
            <p>Education</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/digital.png" class="item-image digital" alt="Digital" />
            <p>Digital</p>
          </div>
      </div>
    </div>
  </div>

  <div class="row justify-content-center value-section">
    <div class="col-12 value-header">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-10">
          <h3>Value</h3>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-10 align-self-center">
      <div class="row no-gutters align-items-start value-items">
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/value_first_icon.png" class="item-image" alt="" />

          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/operational_excellence.png" class="item-image" alt="" />
            <p>Operational<br>Excellence</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/product_leadership.png" class="item-image" alt="" />
            <p>Product Leadership</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/competitive_rates.png" class="item-image" alt="" />
            <p>Competitive Rates</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/performance.png" class="item-image" alt="" />
            <p>Performance</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/customization.png" class="item-image" alt="" />
            <p>Customization</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/quantitative_niche_expertise.png" class="item-image" alt="" />
            <p>Qualitative - <br>Niche Expertise</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/quantitative_cost_structure.png" class="item-image" alt="" />
            <p>Quantitative - <br>Cost Structure / Velocity</p>
          </div>
          <div class="col-12 col-sm-4">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/execution.png" class="item-image" alt="" />
            <p>Execution</p>
          </div>
      </div>
    </div>
  </div>
</main>