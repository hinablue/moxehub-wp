<?php
/**
 * The works
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
?>
<main class="container-fluid works" data-sticky-wrap>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10">
        <h1>Projects</h1>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-12 col-lg-10">
        <div class="card work-card">
          <div class="row">
            <div class="col-12 col-md-6 work-card-image" style="background-image:url(<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/work_form_image.png)"></div>
            <div class="col-12 col-md-6 work-card-information">
              <form class="works-form row align-items-center" id="works-form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="POST">
                <input type="hidden" name="action" value="login">
                <div class="col-12">
                  <div class="form-group">
                      <label for="user-name" class="bmd-label-floating">Username</label>
                      <input type="text" class="form-control" id="user-id" name="id" required>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                      <label for="user-name" class="bmd-label-floating">Password</label>
                      <input type="password" class="form-control" id="user-password" name="password" required>
                  </div>
                </div>
                <div class="col-12 submit-button-container">
                  <div class="alert alert-danger" id="error-message">
                    <div class="container">
                      <div class="row">
                        <div class="col-1">
                          <div class="alert-icon">
                              <i class="material-icons">error_outline</i>
                          </div>
                        </div>
                        <div class="col-10">
                          <b>Error Alert:</b>
                          <span class="error-message"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-link submit-button" id="submit-button">
                    <span class="send-text">Login</span>
                    <div class="loading-icon">
                      <svg class="spinner" width="174px" height="174px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                        <circle class="path" fill="transparent" stroke-width="2" cx="33" cy="33" r="30" stroke="url(#gradient)"/>
                          <linearGradient id="gradient">
                            <stop offset="50%" stop-color="#96822F" stop-opacity="1"/>
                            <stop offset="65%" stop-color="#96822F" stop-opacity=".5"/>
                            <stop offset="100%" stop-color="#96822F" stop-opacity="0"/>
                          </linearGradient>
                        </circle>
                      </svg>
                    </div>
                    <div class="succeeded-icon"></div>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>