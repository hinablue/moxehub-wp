<?php
/**
 * The services
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */

$content = get_the_content();
$description = '';
$services_item = [];

if (preg_match('#<h3>Description</h3>\s*(.*?)\s*<ul>#im', $content, $m)) {
    $description = $m[1];
}
if (preg_match_all('#<li>(?s)(.*?)</ul>#im', $content, $matches)) {
    for ($i = 0, $m = $matches[1], $l = count($matches[1]); $i < $l; $i += 2) {
        preg_match('#<h3>(.*?)</h3>#im', $m[$i], $title);
        preg_match_all('#<li>(.*?)</li>#im', $m[$i], $list);
        array_pop($list[0]);

        preg_match('#<img(?:.*?)src="(.*?)"#im', $m[($i + 1)], $image);
        array_push(
            $services_item,
            [
                'key' => str_replace(['  ', ' '], '-', preg_replace('/[^a-z0-9\-\s]*/i', '', strtolower($title[1]))),
                'title' => $title[1],
                'list' => implode('', $list[0]),
                'description' => array_pop($list[1]),
                'image' => preg_replace('#-[0-9]+x[0-9]+#i', '', $image[1])
            ]
        );
    }

    $inject_styles = '';
    foreach ($services_item as $item) {
        $inject_styles .= 'main.services .services-item.' . $item['key'] . ',';
        $inject_styles .= 'main.services .services-modal.' . $item['key'] . '::before {';
        $inject_styles .= 'background-image: url("' . $item['image'] . '"); }'.PHP_EOL;
    }
    // Overwrite service items background.
    echo '<style type="text/css" media="screen">' . $inject_styles . '</style>';
}
?>
<main class="container services" data-sticky-wrap>
  <div class="row justify-content-center">
    <div class="col-12">
      <h1><?php echo get_the_title(); ?></h1>
      <div class="row">
        <div class="col-12 col-md-8">
          <section class="description"><?php echo $description; ?></section>
        </div>
      </div>
    </div>
    <?php foreach ($services_item as $item) { ?>
    <div class="col-12 col-lg-6 services-item <?php echo $item['key']; ?>">
      <div class="row">
        <div class="col-12 services-item-data">
          <h2><?php echo $item['title']; ?></h2>
          <ul><?php echo $item['list'];?></ul>
        </div>
        <div class="col-12 align-self-end">
          <button type="button" class="btn btn-link detail-button" data-target="<?php echo $item['key']; ?>">
            <span>Learn More</span>
          </button>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <div class="row justify-content-center services-modals">
    <?php foreach ($services_item as $item) { ?>
    <div class="col-12 services-modal <?php echo $item['key']; ?>">
      <div class="row justify-content-center">
        <div class="col-10">
          <h2><?php echo $item['title']; ?></h2>
          <p class="description"><?php echo $item['description']; ?></p>
          <ul><?php echo $item['list'];?></ul>
        </div>
      </div>
      <button type="button" class="btn btn-fab btn-round close-modal-button">
        <i class="material-icons">close</i>
      </button>
    </div>
    <?php } ?>
  </div>
</main>