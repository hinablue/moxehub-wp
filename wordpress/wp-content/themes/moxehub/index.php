<?php
/**
 * The main template file
 *
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
get_header(); ?>

<?php
if (is_home() && is_front_page()) {
    get_template_part('template-parts/page/content', 'home');
}
?>
<?php get_footer(); ?>