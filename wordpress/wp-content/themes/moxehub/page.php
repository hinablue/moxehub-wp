<?php
/**
 * The page file
 *
 * PHP Version 7.0
 *
 * @category   Theme
 * @package    WordPress
 * @subpackage MoxeHub
 * @author     Hina Chen <hinablue@gmail.com>
 * @license    MIT https://opensource.org/licenses/MIT
 * @link       http://moxhub.com
 * @since      MoxeHun 1.0
 */
get_header();

if (have_posts()) {
    $slug = get_post_field('post_name', get_post());
    if (in_array($slug, ['contact', 'services', 'works'], true)) {
        the_post();
        get_template_part('template-parts/page/content', $slug);
    } else {
        get_template_part('template-parts/page/content');
    }
} else {
    get_template_part('template-parts/page/content');
}
get_footer(); ?>